# University Class Notes

This repository has the class notes, programs, assignments etc. of my current university semester. 

Papers for the current semesters:

- Software Engineering
- Theory of Computation
- Embedded Systems
- Introduction to Artificial Intelligence
